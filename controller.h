#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "bbdd.h"

class controller : public bbdd
{
public:
    QSqlDatabase db;
    bbdd dbcontroller;
    controller();
    bool createTable();
    bool addData(QString a);
    QSqlTableModel* getmodel(QString name);
};

#endif // CONTROLLER_H
