#include "controller.h"
#include <iostream>
controller::controller()
{
    std::cout<<"Aplicacion iniciada..."<<std::endl;

    QString nombre;
    nombre.append("BaseDeDatos1.sqlite");

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(nombre);

    if(db.open()){
        std::cout<<"Se ha conectado a la base de datos"<<std::endl;
    }else{
        std::cout<<"No se ha conectado a la base de datos"<<std::endl;
    }

}

QSqlTableModel* controller::getmodel(QString name){
    QSqlTableModel *model = new QSqlTableModel;
    model->setTable(name);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    return model;
}

bool controller::createTable(){
    dbcontroller.createTable();
    return true;
}
bool controller::addData(QString a){
    dbcontroller.addData(a);
    return true;
}

