#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    crearTablaUsuarios();

    model = controlador.getmodel("usuarios");

    ui->tableView->setModel(model);

    model->select();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::crearTablaUsuarios()
{
    controlador.createTable();
}

void MainWindow::insertarUsuarios()
{
    QString nombre = ui->lineEdit->text();
    if(controlador.addData(nombre)!=0){
        printf("bien\n");
    }else{
        printf("mal\n");
    }
}
void MainWindow::on_pushButton_clicked()
{
    //insertarUsuarios();

    QSqlRecord r = model->record();
    r.setValue("nombre", "name");
    r.setValue("apellido", "apellido");
    r.setValue("edad", 30);
    r.setValue("clase", 1);
    QString date =  ui->dateEdit->date().toString();
    r.setValue("fecha", date);
    model->insertRecord(-1, r);
    model->submitAll();
    model->select();
}
