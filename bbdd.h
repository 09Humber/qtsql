#ifndef BBDD_H
#define BBDD_H
#include <QObject>
#include <QSqlQuery>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QSqlRecord>
class bbdd
{
public:
    bbdd();
    bool createTable();
    bool addData(QString a);
    QSqlQuery getData();
};


#endif // BBDD_H
