#include "bbdd.h"
#include <iostream>
bbdd::bbdd()
{

}

bool bbdd::createTable()
{
    QString consulta;
    consulta.append("CREATE TABLE IF NOT EXISTS usuarios("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "nombre VARCHAR(100),"
                    "apellido VARCHAR(100),"
                    "edad INTEGER NOT NULL,"
                    "clase INTEGER NOT NULL,"
                    "fecha DATE"
                    ");");
    QSqlQuery crear;
    crear.prepare(consulta);

    if(crear.exec()){
        std::cout<<"Ya existe la tabla usuarios"<<std::endl;
    }else{
        std::cout<<"No existe la tabla usuarios o no se ha creado correctamente"<<std::endl;
    }
    return true;
}

bool bbdd::addData(QString a)
{
    QString consulta;
    consulta.append("INSERT INTO usuarios("
                    "nombre,"
                    "apellido,"
                    "edad,"
                    "clase)"
                    "VALUES("
                    "'"+a+"',"
                    "'pepe',"
                    "22,"
                    "1985"
                    "1-1-2020"
                    ");");
    QSqlQuery insertar;
    insertar.prepare(consulta);

    if(insertar.exec()){
        std::cout<<"Se inserto correctamente"<<std::endl;
    }else{
        std::cout<<"No se inseerto correctamente"<<std::endl;
    }

    return true;
}

QSqlQuery bbdd::getData()
{
    QString mostrar;
    mostrar.append("SELECT * FROM usuarios");
    QSqlQuery consultar;
    consultar.prepare(mostrar);

    if(consultar.exec()){
        std::cout<<"Se mostro correctamente"<<std::endl;
    }else{
        std::cout<<"No se mostro correctamente"<<std::endl;
    }

   return consultar;

}


